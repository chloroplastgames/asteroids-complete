﻿using System;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{

    public event Action<int> OnScoreChange = delegate { };

    private int _totalScore = 0;

    public int TotalScore
    {
        get { return _totalScore; }
        private set { _totalScore = value; }
    }


    private void OnEnable()
    {
        ScoreBehavior.OnScore += OnScoreHandler;
    }

    private void OnDisable()
    {
        ScoreBehavior.OnScore -= OnScoreHandler;
    }

    private void OnScoreHandler(int score)
    {
        _totalScore += score;

        OnScoreChange.Invoke(_totalScore);
    }
}
