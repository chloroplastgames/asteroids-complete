﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ScoreDataBase : ScriptableObject
{
    [SerializeField]
    protected int _currtentScore;

    public async void SetCurrentScore(int score)
    {
        _currtentScore = score;
    }

    public int GetCurrentScore()
    {
        return _currtentScore;
    }

    public abstract void AddScore(int score);

    public abstract System.Threading.Tasks.Task<int[]> GetTopScores(int total);

}
