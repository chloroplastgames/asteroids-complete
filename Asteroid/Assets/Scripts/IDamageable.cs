﻿public interface IDamageable
{
    void Hurt(int amount);
}
