﻿using UnityEngine;

public class PowerUpController : MonoBehaviour
{


    private DestroyBehavior _destroyer;
    private ContactBase _contact;
    private EngineBehavior _engine;
    private void Awake()
    {
        _destroyer = GetComponent<DestroyBehavior>();
        _contact = GetComponent<ContactBase>();
        _engine = GetComponent<EngineBehavior>();
    }

    private void OnEnable()
    {
        _contact.OnContact += OnContactHandler;

    }

    private void OnDisable()
    {
        _contact.OnContact -= OnContactHandler;
    }

    private void FixedUpdate()
    {
        _engine.Move(Vector3.down);
    }

    private void OnContactHandler(Collider obj)
    {
        _destroyer.DestroyObject();
    }
}
