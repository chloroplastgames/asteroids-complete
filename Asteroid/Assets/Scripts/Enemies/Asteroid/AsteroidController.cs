﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidController : MonoBehaviour
{
    private EngineBehavior _engine;
    private HealthBehavior _health;
    private DestroyBehavior _destroyer;
    private ScoreBehavior _score;

    private void Awake()
    {
        _engine = GetComponent<EngineBehavior>();
        _health = GetComponent<HealthBehavior>();
        _destroyer = GetComponent<DestroyBehavior>();
        _score = GetComponent<ScoreBehavior>();
    }   

    private void OnEnable()
    {
        _health.OnDead += OnDeadHandler;

    }

    private void OnDisable()
    {
        _health.OnDead -= OnDeadHandler;
    }

    private void FixedUpdate()
    {
        _engine.Move(Vector3.down);
    }

    private void OnDeadHandler(GameObject obj)
    {
        _score.DoScore();
        _destroyer.DestroyObject();        
    }
}
