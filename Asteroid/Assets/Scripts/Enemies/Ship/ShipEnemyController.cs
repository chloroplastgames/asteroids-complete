﻿using System.Collections;
using UnityEngine;

public class ShipEnemyController : MonoBehaviour
{  
    
    private PathMovement _engine;
    private HealthBehavior _health;
    private ILaunch _launcher;
    private DestroyBehavior _destroyer;
    private ScoreBehavior _score;

    private void Awake()
    {
        _engine = GetComponent<PathMovement>();
        _health = GetComponent<HealthBehavior>();
        _launcher = GetComponent<ILaunch>();
        _destroyer = GetComponent<DestroyBehavior>();
        _score = GetComponent<ScoreBehavior>();
    }

    private void OnEnable()
    {
        _health.OnDead += OnDeadHandler;

        _engine.StartMovement();

        StartCoroutine(LaunchProjectile());
    }

    private void OnDisable()
    {
        _health.OnDead -= OnDeadHandler;

        StopAllCoroutines();
    }

    private void OnDeadHandler(GameObject obj)
    {
        _score.DoScore();
        _destroyer.DestroyObject();
    }

    private IEnumerator LaunchProjectile()
    {
        yield return new WaitForSeconds(0.75f);

        _launcher.Launch();

        yield return LaunchProjectile();
    } 

}
