﻿using System.Collections;
using UnityEngine;

public class MineController : MonoBehaviour
{
    [SerializeField]
    private float _min;
    [SerializeField]
    private float _max;


    private DestroyBehavior _destroyer;

    private EngineBehavior _engine;

    private ExplosionDamageBehavior _explosion;

    private HealthBehavior _health;
    private ScoreBehavior _score;

    private void Awake()
    {
        _destroyer = GetComponent<DestroyBehavior>();
        _engine = GetComponent<EngineBehavior>();
        _explosion = GetComponent<ExplosionDamageBehavior>();
        _health = GetComponent<HealthBehavior>();
        _score = GetComponent<ScoreBehavior>();
    }

    private void Start()
    {
        StartCoroutine(DoWaitForDestroy());
    }

    private void OnEnable()
    {
        _health.OnDead += OnDeadHandler;
    }



    private void OnDisable()
    {
        _health.OnDead -= OnDeadHandler;
    }



    private IEnumerator DoWaitForDestroy()
    {
        float randomSeconds = Random.Range(_min, _max);

        yield return new WaitForSeconds(randomSeconds);

        Explosion();

    }

    private void Explosion()
    {
        _explosion.DoExplosion();

        _destroyer.DestroyObject();
    }

    private void FixedUpdate()
    {
        _engine.Move(Vector3.down);
    }

    private void OnDeadHandler(GameObject obj)
    {
        _score.DoScore();
        Explosion();
    }

}
