﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Threading.Tasks;

[CreateAssetMenu(menuName ="Data/Score")]
public class ScoreData : ScoreDataBase
{
    [SerializeField]
    private List<int> _scores = new List<int>();

    public override void AddScore(int score)
    {
        _scores.Add(score);
    }

    public override async Task<int[]> GetTopScores(int total)
    {
        return _scores.
            OrderByDescending(score => score).
            Take(total).
            ToArray();
    }

}
