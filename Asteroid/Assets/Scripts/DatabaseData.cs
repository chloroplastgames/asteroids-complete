﻿using UnityEngine;

[CreateAssetMenu(menuName ="Data/Database")]
public class DatabaseData : ScriptableObject
{
    [SerializeField]
    private string _server;
    [SerializeField]
    private string _port;
    [SerializeField]
    private string _database;
    [SerializeField]
    private string _user;
    [SerializeField]
    private string _password;


    public string GetConnectionInfo()
    {
        return $"Server={_server};Database={_database};Uid={_user};Pwd={_password};CharSet=utf8;Port={_port}";
    }
}
