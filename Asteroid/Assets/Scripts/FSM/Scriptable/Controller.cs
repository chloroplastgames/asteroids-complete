﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FSM.Scriptable
{
    public class Controller : MonoBehaviour
    {

        public State curretState;
        public State remainState;

        public EngineBehavior Engine { get; private set; }

        public ILaunch Launcher { get; private set; }

        public HealthBehavior Health { get; private set; }

        public DestroyBehavior Destroyer { get; private set; }

        public bool ActiveAI { get; set; }

        private Animator _animator;

        private void Awake()
        {
            Engine = GetComponent<EngineBehavior>();

            Launcher = GetComponent<ILaunch>();

            Health = GetComponent<HealthBehavior>();

            Destroyer = GetComponent<DestroyBehavior>();

            _animator = GetComponent<Animator>();
        }

        private void Start()
        {
            ActiveAI = true;
        }

        private void Update()
        {
            if (!ActiveAI) return;

            curretState.UpdateState(this);
        }

        public void SetAnimation(string animation, bool value)
        {
            _animator.SetBool(animation, value);
        }

        public void Transition(State nextState)
        {
            if (nextState != remainState)
            {
                curretState = nextState;
            }
        }
    }
}

