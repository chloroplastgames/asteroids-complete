﻿public interface IState
{
    void Update();
    void Enter();
    void Exit();
}
