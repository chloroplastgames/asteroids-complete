﻿using UnityEngine;

public abstract class Controller : MonoBehaviour, IController
{
    public IState CurrentState { get; set; }
    public IState LastState { get; set; }


    protected virtual void Update()
    {
        if (CurrentState != null)
        {
            CurrentState?.Update();
        }

    }

    protected virtual void Init(IState state)
    {
        CurrentState = state;
        CurrentState.Enter();
    }

    public void SwitchState(IState state)
    {
        LastState = CurrentState;
        CurrentState.Exit();
        state.Enter();
        CurrentState = state;
    }
}
