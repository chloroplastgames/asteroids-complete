﻿public interface IController
{
    IState CurrentState { get; set; }
    IState LastState { get; set; }

    void SwitchState(IState state);
}
