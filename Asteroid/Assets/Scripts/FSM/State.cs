﻿using UnityEngine;

public abstract class State<T> : IState where T : IController
{
    protected T Controller { get; private set; }

    public State(T controller)
    {
        Controller = controller;
    }

    public virtual void Update()
    {

    }

    public virtual void Enter()
    {
        #if UNITY_EDITOR
        Debug.Log($"Enter ---> {this.GetType()}");
        #endif

    }

    public virtual void Exit()
    {
        #if UNITY_EDITOR
        Debug.Log($"Exit ---> {this.GetType()}");
        #endif
    }
}
