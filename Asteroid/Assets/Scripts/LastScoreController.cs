﻿using UnityEngine;
using UnityEngine.UI;

public class LastScoreController : MonoBehaviour
{
    [SerializeField]
    private GameObject _scorePrefab;

    [SerializeField]
    private Text _currentScore;

    [SerializeField]
    private int _total;

    [SerializeField]
    private ScoreDataBase _scoreData;

    private void Start()
    {
        SetScores(_total);
    }

    private async void SetScores(int total)
    {
        _currentScore.text = $"{_scoreData.GetCurrentScore()}";

        var scores = await _scoreData.GetTopScores(total);

        for (int i = 0;
            i < scores.Length;
            i++)
        {
            Instantiate(_scorePrefab, transform).
                GetComponent<LastScoreCanvasBehavior>().
                SetScore(i + 1, scores[i]);
        }
    }
}
