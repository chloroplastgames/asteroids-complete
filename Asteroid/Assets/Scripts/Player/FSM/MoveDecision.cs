﻿using FSM.Scriptable;
using System;
using UnityEngine;

[CreateAssetMenu(menuName ="PluggableAI/Decisions/Move")]
public class MoveDecision : Decision
{
    public override bool Decide(FSM.Scriptable.Controller controller)
    {
        return Math.Abs(Input.GetAxis("Horizontal")) > 0;
    }
}
