﻿using UnityEngine;
using FSM.Scriptable;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/Shoot")]
public class ShootDecision : Decision
{
    public override bool Decide(FSM.Scriptable.Controller controller)
    {
        return Input.GetKeyDown(KeyCode.Space);
    }
}
