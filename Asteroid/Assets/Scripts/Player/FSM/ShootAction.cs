﻿using UnityEngine;
using FSM.Scriptable;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Shoot")]
public class ShootAction : Action
{
    public override void Act(FSM.Scriptable.Controller controller)
    {
        controller.Launcher.Launch();
    }
}
