﻿using FSM.Scriptable;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Move")]
public class MoveAction : Action
{
    public override void Act(FSM.Scriptable.Controller controller)
    {
        var direction = new Vector3(Input.GetAxis("Horizontal"), 
                                    0, 
                                    0);
        controller.SetAnimation("Left", direction.x < 0);
        controller.SetAnimation("Right", direction.x > 0);

        controller.Engine.Move(direction);
    }
}