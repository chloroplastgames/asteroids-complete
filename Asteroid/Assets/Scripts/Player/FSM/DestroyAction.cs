﻿using FSM.Scriptable;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Destroy")]
public class DestroyAction : Action
{
    public override void Act(FSM.Scriptable.Controller controller)
    {
        var scoreManager = FindObjectOfType<ScoreManager>();
        GameController.Instance.GameOver(scoreManager.TotalScore);
        controller.Destroyer.DestroyObject();
    }
}
