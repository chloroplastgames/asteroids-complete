﻿using FSM.Scriptable;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/Destroy")]
public class DestroyDecision : Decision
{
    public override bool Decide(FSM.Scriptable.Controller controller)
    {
        return controller.Health.IsDead();
    }
}
