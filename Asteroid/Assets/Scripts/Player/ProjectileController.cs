﻿using UnityEngine;

public class ProjectileController : MonoBehaviour
{

    private HealthBehavior _health;

    private DestroyBehavior _destroyer;

    private void Awake()
    {
        _health = GetComponent<HealthBehavior>();

        _destroyer = GetComponent<DestroyBehavior>();
    }

    private void OnEnable()
    {
        _health.OnDead += OnDeadHandler;
    }

    private void OnDisable()
    {
        _health.OnDead -= OnDeadHandler;
    }

    private void OnDeadHandler(GameObject obj)
    {
        _destroyer.DestroyObject();
    }
}
