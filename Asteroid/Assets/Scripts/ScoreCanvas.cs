﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCanvas : MonoBehaviour
{
    private Text _text;
    private ScoreManager _scoreManager;

    private void Awake()
    {
        _text = GetComponent<Text>();

        _scoreManager = FindObjectOfType<ScoreManager>();
    }

    private void OnEnable()
    {
        _scoreManager.OnScoreChange += OnScoreChangeHandler;
    }
    private void OnDisable()
    {
        _scoreManager.OnScoreChange -= OnScoreChangeHandler;
    }

    private void OnScoreChangeHandler(int score)
    {
        _text.text = $"{score}";
    }

}
