﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;

[CreateAssetMenu(menuName = "MySql/Connection")]
public class MySQL : ScoreDataBase
{

    [SerializeField]
    private DatabaseData _databaseInfo;

    public override async void AddScore(int score)
    {
        try
        {
            string query = $"INSERT INTO score VALUES (NULL, {score})";

            MySqlConnection conn =
                new MySqlConnection(_databaseInfo.
                GetConnectionInfo());

            await conn.OpenAsync();
            
            var command = new MySqlCommand(query, conn);

            await command.ExecuteReaderAsync();
            
            conn.Close();

        }
        catch (MySqlException ex)
        {

            throw ex;
        }
    }

    public override async Task<int[]> GetTopScores(int total)
    {
        try
        {
            string query = $"SELECT * from score ORDER BY score DESC LIMIT {total}";

            MySqlConnection conn =
                new MySqlConnection(
                    _databaseInfo.
                    GetConnectionInfo());

            var command = new MySqlCommand(query, conn);

            await conn.OpenAsync();

            var data = await command.ExecuteReaderAsync();

            var scores = new List<int>();
            
            while (await data.ReadAsync())
            {
                scores.Add(data.GetInt32(1));
            }
            

            return scores.ToArray();
        }
        catch (MySqlException ex)
        {
            throw ex;
        }
    }


}
