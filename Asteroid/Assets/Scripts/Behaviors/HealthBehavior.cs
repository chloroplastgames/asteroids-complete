﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HealthBehavior : MonoBehaviour, IDamageable, IHeal
{

    public event Action<GameObject> OnDead = delegate { };

    public event Action<int> OnHealthChange = delegate { };

    public int Health { get { return _health; } private set { _health = value; } }

    [SerializeField]
    private int _health;

    [SerializeField]
    private int _currentHealth;

    private void OnEnable()
    {
        _currentHealth = _health;
    }


    public bool IsDead()
    {
        return _currentHealth < 1;
    }

    public void Heal(int amount)
    {
        if(amount > 0)
        {
            _currentHealth = Math.Min(_health,
                _currentHealth + amount);
            OnHealthChange.Invoke(_currentHealth);
        }

    }

    public void Hurt(int amount)
    {
        if(amount > 0)
        {
            _currentHealth -= amount;

            OnHealthChange.Invoke(_currentHealth);

            if (_currentHealth <=0)
            {
                OnDead.Invoke(gameObject);
            }
        }
    }
}
