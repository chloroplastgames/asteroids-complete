﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ExitButtonBehavior : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        GameController.Instance.ExitGame();
    }
}
