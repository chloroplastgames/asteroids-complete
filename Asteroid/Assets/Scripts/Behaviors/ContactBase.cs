﻿using System;
using UnityEngine;

public abstract class ContactBase : MonoBehaviour
{

    public event Action<Collider> OnContact = delegate { };

    [SerializeField]
    protected LayerMask _layer;

    protected virtual void OnTriggerEnter(Collider other)
    {
        bool makeContact = (_layer.value & 1 << other.gameObject.layer)
                    == 1 << other.gameObject.layer;

        if (makeContact)
        {           
            DoContactAction(other);

            OnContact.Invoke(other);
        }
        
    }

    protected virtual void DoContactAction(Collider other)
    {

    }
}
