﻿using UnityEngine;

public abstract class DamageBase : ContactBase
{
    [SerializeField]
    private int _damage;


    public virtual void DoDamage(Collider other)
    {
        IDamageable target = other.GetComponent<IDamageable>();

        bool makeDamage = (_layer.value & 1 << other.gameObject.layer)
                            == 1 << other.gameObject.layer;
        if (makeDamage && target != null)
        {
            print($" {gameObject.name} make damage to {other.gameObject.name}");

            target.Hurt(_damage);
        }
    }
}
