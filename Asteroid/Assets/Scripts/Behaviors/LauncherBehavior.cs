﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LauncherBehavior : MonoBehaviour, ILaunch
{
    [SerializeField]
    private Vector3 _direction;

    [SerializeField]
    private float _force;

    [SerializeField]
    private Transform _pointer;

    [SerializeField]
    private float _fireRate;

    [SerializeField]
    private GameObject _projectile;

    private bool _canShoot;


    private void OnEnable()
    {
        _canShoot = true;
    }

    public void Launch()
    {
        if (_canShoot)
        {
            var projectile = ObjectPoolingManager.Instance.
                InstantiatePoolItem(
                _projectile,
                _pointer.position, 
                Quaternion.identity);

            var projectileRB = projectile.GetComponent<Rigidbody>();

            projectileRB.velocity = _direction * _force;

            StartCoroutine(DoFireRate());
        }
    }

    private IEnumerator DoFireRate()
    {
        _canShoot = false;

        yield return new WaitForSeconds(_fireRate);

        _canShoot = true;
    }
}
