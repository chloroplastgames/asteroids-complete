﻿using UnityEngine;
using UnityEngine.EventSystems;

public class PauseButtonBehavior : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        GameController.Instance.PauseGame();
    }
}
