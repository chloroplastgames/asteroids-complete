﻿using UnityEngine;
using UnityEngine.UI;
public class LastScoreCanvasBehavior : MonoBehaviour
{
    [SerializeField]
    private Text _index;

    [SerializeField]
    private Text _score;


    public void SetScore(int index, int score)
    {
        _index.text = $"{index} - ";
        _score.text = $"{score}";
    }
}
