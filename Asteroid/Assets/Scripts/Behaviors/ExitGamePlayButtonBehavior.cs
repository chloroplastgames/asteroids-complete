﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ExitGamePlayButtonBehavior : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        GameController.Instance.ExitGamePlay();
    }
}
