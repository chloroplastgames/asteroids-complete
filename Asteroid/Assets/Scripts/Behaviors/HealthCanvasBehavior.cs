﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthCanvasBehavior : MonoBehaviour
{

    private HealthBehavior _healthBehavior;

    private Image _image;

    private void Awake()
    {
        _healthBehavior = GameObject.
                        FindGameObjectWithTag("Player").
                        GetComponent<HealthBehavior>();

        _image = GetComponent<Image>();
    }

    private void OnEnable()
    {
        _healthBehavior.OnHealthChange += OnHealthChangeHandler;
    }


    private void OnDisable()
    {
        _healthBehavior.OnHealthChange -= OnHealthChangeHandler;
    }

    private void OnHealthChangeHandler(int health)
    {

        _image.fillAmount = (float)health / _healthBehavior.Health;
    }
}
