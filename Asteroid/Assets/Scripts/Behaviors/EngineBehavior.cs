﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class EngineBehavior : MonoBehaviour
{
    [SerializeField]
    private float _speed;

    private Rigidbody _rigidbody;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    public virtual void Move(Vector3 direction)
    {
        _rigidbody.MovePosition(transform.position + direction * _speed * Time.fixedDeltaTime);
    }

}
