﻿using UnityEngine;

public class HealBehavior : ContactBase
{
    [SerializeField]
    private int _healAmount = 1;

    protected override void DoContactAction(Collider other)
    {
        base.DoContactAction(other);

        var target = other.GetComponent<IHeal>();

        if (target != null)
        {
            target.Heal(_healAmount);
        }
    }
}
