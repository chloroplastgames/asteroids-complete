﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ResumeButtonBehavior : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        GameController.Instance.ResumeGame();
    }
}
