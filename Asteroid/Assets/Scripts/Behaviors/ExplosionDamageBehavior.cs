﻿using UnityEngine;

public class ExplosionDamageBehavior : DamageBase
{
    [SerializeField]
    private float _radius;

    public void DoExplosion()
    {
        Collider[] others = Physics.OverlapSphere(transform.position, _radius);

        for (int i = 0; i < others.Length; i++)
        {
            DoDamage(others[i]);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(1, 0, 0, 0.25f);
        Gizmos.DrawSphere(transform.position, _radius);
    }

}
