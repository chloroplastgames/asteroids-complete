﻿using UnityEngine;
using UnityEngine.EventSystems;

public class PlayButtonBehavior : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        GameController.Instance.PlayGame();
    }

}
