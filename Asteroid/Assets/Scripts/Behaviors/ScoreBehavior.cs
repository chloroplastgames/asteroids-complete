﻿using System;
using UnityEngine;

public class ScoreBehavior : MonoBehaviour
{

    public static event Action<int> OnScore = delegate { };

    [SerializeField]
    private int _score;

    public void DoScore()
    {
        OnScore.Invoke(_score);
    }

}
