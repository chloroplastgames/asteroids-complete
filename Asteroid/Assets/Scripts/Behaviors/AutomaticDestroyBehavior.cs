﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomaticDestroyBehavior : DestroyBehavior
{
    [SerializeField]
    private float _delay;

    private void OnEnable()
    {

        StartCoroutine(StartDestroy());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private IEnumerator StartDestroy()
    {
        yield return new WaitForSeconds(_delay);

        DestroyObject();
    }
}
