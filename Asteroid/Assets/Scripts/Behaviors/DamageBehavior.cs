﻿using UnityEngine;

public class DamageBehavior : DamageBase
{
    protected override void DoContactAction(Collider other)
    {
        base.DoContactAction(other);
        DoDamage(other);
    }
}
