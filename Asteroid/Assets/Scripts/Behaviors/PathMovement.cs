﻿using System.Collections;
using UnityEngine;

public class PathMovement : EngineBehavior
{

    [SerializeField]
    private float _waitTransicion;

    [SerializeField]
    private Vector3[] _points;

    private int _currentPoint = 0;


    public void StartMovement()
    {
        StartCoroutine(DoMovement(NextPoint()));
    }

    private IEnumerator DoMovement(Vector3 point)
    {

        yield return new WaitForSeconds(_waitTransicion);

        while (Vector3.Distance(transform.position, point) > 0.1f)
        {
            Vector3 direction = point - transform.position;

            Move(direction.normalized);

            yield return null;

        }

        yield return DoMovement(NextPoint());

    }

    private Vector3 NextPoint()
    {
        int next = (_currentPoint + 1) % _points.Length;

        _currentPoint = next;

        return _points[next];
    }


}
