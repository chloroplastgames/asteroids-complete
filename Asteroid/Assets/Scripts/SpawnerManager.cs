﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Serie
{
    public float starDelay;
    public float endDelay;
    public Element[] elements;
}

[Serializable]
public class Element
{
    public float delay;

    public GameObject prefab;

    public int total;
}


public class SpawnerManager : MonoBehaviour
{
    [SerializeField]
    private Transform[] _points;

    [SerializeField]
    private Serie[] _series;


    private void Start()
    {
        StartSpawn();
    }

    public void StartSpawn()
    {
        StartCoroutine(DoSpawn());
    }

    private IEnumerator DoSpawn()
    {
        foreach (var serie in _series)
        {
            yield return DoSpawnSerie(serie);
        }

        yield return DoSpawn();

    }

    private IEnumerator DoSpawnSerie(Serie serie)
    {
        yield return new WaitForSeconds(serie.starDelay);

        yield return DoSpawnElements(serie.elements);

        yield return new WaitForSeconds(serie.endDelay);
    }

    private IEnumerator DoSpawnElements(Element[] elements)
    {
        foreach (var element in elements)
        {
            yield return DoSpawnElement(element);
        }
    }

    private IEnumerator DoSpawnElement(Element element)
    {
        for (int i = 0; i < element.total; i++)
        {
            yield return new WaitForSeconds(element.delay);

            ObjectPoolingManager.Instance.
                InstantiatePoolItem(
                element.prefab,
                GetRandomSpawnPosition(),
                element.prefab.transform.rotation);
        }
    }

    private Vector3 GetRandomSpawnPosition()
    {
        var randomPosition = UnityEngine.Random.Range(0, _points.Length);

        return _points[randomPosition].position;
    }

}
