﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GameController : Controller
{
    private static GameController m_Instance;

    public static GameController Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = FindObjectOfType<GameController>();
                if (m_Instance == null)
                {
                    var singletonObject = new GameObject();
                    m_Instance = singletonObject.AddComponent<GameController>();
                    singletonObject.name = typeof(GameController).ToString() + " (Singleton)";

                    DontDestroyOnLoad(singletonObject);
                }
            }
            return m_Instance;

        }
    }


    public ScoreDataBase ScoreData { 
        get { return _scoreData; }
        private set { _scoreData = value; }
    }    

    public event Action OnPlayGame = delegate { };

    public event Action OnPauseGame = delegate { };

    public event Action OnResumeGame = delegate { };

    public event Action OnExitGame = delegate { };

    public event Action OnGameOver = delegate { };

    [SerializeField]
    private ScoreDataBase _scoreData;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        Init(new LogoState(this));
    }


    public void PauseGame()
    {
        OnPauseGame.Invoke();

    }

    public void ResumeGame()
    {
        OnResumeGame.Invoke();
    }

    public void ExitGamePlay()
    {
        OnExitGame.Invoke();
    }

    public void SetGameTime(float value)
    {
        Time.timeScale = value;
    }

    public void GameOver(int score)
    {
        ScoreData.SetCurrentScore(score);
        ScoreData.AddScore(score);
        OnGameOver.Invoke();
    }

    public void PlayGame()
    {
        OnPlayGame.Invoke();
    }

    public void ExitGame()
    {
        Application.Quit();
    }

}
