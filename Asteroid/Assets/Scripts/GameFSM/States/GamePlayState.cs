﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GamePlayState : State<GameController>
{
    public GamePlayState(GameController controller) : base(controller)
    {

    }

    public override void Enter()
    {
        base.Enter();

        if (!(Controller.LastState is PauseState))
        {
            SceneManager.LoadScene("GamePlay");
        }

        Controller.OnPauseGame += OnPauseGameHandler;
        Controller.OnGameOver += OnGameOverHandler;

    }



    public override void Exit()
    {
        base.Exit();
        Controller.OnPauseGame -= OnPauseGameHandler;
        Controller.OnGameOver -= OnGameOverHandler;
    }

    private void OnGameOverHandler()
    {
        Controller.SwitchState(new GameOverState(Controller));
    }

    private void OnPauseGameHandler()
    {
        Controller.SwitchState(new PauseState(Controller));
    }
}
