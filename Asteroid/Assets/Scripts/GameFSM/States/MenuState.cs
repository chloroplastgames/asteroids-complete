﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuState : State<GameController>
{
    public MenuState(GameController controller) : base(controller)
    {

    }

    public override void Enter()
    {
        base.Enter();        
        SceneManager.LoadScene("Menu");
        Controller.OnPlayGame += OnPlayGameHandler;
    }

    public override void Exit()
    {
        base.Exit();
        Controller.OnPlayGame -= OnPlayGameHandler;
    }

    private void OnPlayGameHandler()
    {
        Controller.OnPlayGame -= OnPlayGameHandler;
        Controller.SwitchState(new GamePlayState(Controller));
    }

}
