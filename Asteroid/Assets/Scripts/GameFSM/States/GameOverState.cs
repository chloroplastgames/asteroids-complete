﻿using UnityEngine.SceneManagement;

public class GameOverState : State<GameController>
{
    public GameOverState(GameController controller) : base(controller)
    {
    }

    public override void Enter()
    {
        base.Enter();

        SceneManager.LoadScene("GameOver");

        Controller.OnExitGame += OnExitGameHandler;
    }

    public override void Exit()
    {
        base.Exit();

        Controller.OnExitGame -= OnExitGameHandler;
    }

    private void OnExitGameHandler()
    {
        Controller.SwitchState(new MenuState(Controller));
    }
}
