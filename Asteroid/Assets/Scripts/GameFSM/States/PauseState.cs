﻿public class PauseState : State<GameController>
{
    public PauseState(GameController controller) : base(controller)
    {

    }

    public override void Enter()
    {
        base.Enter();
        Controller.OnResumeGame += OnResumeGameHandler;
        Controller.OnExitGame += OnExitGame;
        Controller.SetGameTime(0);
    }



    public override void Exit()
    {
        base.Exit();
        Controller.OnResumeGame -= OnResumeGameHandler;
        Controller.OnExitGame -= OnExitGame;
        Controller.SetGameTime(1);
    }

    private void OnExitGame()
    {
        Controller.SwitchState(new MenuState(Controller));
    }

    private void OnResumeGameHandler()
    {
        Controller.SwitchState(new GamePlayState(Controller));
    }

}
