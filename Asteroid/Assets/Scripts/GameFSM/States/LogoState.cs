﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class LogoState : State<GameController>
{

    private VideoPlayer _videoPlayer;

    public LogoState(GameController controller) : base(controller) {
        _videoPlayer = Object.FindObjectOfType<VideoPlayer>();
    }


    public override void Enter()
    {
        base.Enter();

        _videoPlayer.loopPointReached += LoopPointReachedHandler;


    }
    public override void Exit()
    {
        base.Exit();

        _videoPlayer.loopPointReached -= LoopPointReachedHandler;
    }

    void LoopPointReachedHandler(VideoPlayer videoPlayer)
    {
        Debug.Log("Video is over");
        Controller.SwitchState(new MenuState(Controller));
    }


}
