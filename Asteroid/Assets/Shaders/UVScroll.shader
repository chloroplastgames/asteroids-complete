// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "CG Framework/Unlit/Unlit Transparent"
{
	Properties
	{
		_MainTex ("Texture (RGBA)", 2D) = "white" {}
		_Tint ("Color (RGBA)", Color) = (1,1,1,1)
		[Toggle(_UVSCROLL_ON)] _UVScroll("UV Scroll", Float) = 0
		[Toggle][Toggle]_FlipUVHorizontal("Flip UV Horizontal", Float) = 0
		[Toggle][Toggle]_FlipUVVertical("Flip UV Vertical", Float) = 0
		[Toggle][Toggle]_UVScrollAnimation("UV Scroll Animation", Float) = 1
		_UVScrollSpeed("UV Scroll Speed", Vector) = (0,0,0,0)
		[Toggle]_ScrollByTexel("Scroll By Texel", Float) = 0
	}
	
	SubShader
	{
		Tags { "RenderType"="Transparent" "Queue"="Transparent" "LightMode"="ForwardBase" }
		LOD 100
		Cull Off
		ZWrite Off
		ZTest LEqual
		

		Pass
		{

			Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off

			CGPROGRAM
			#pragma target 3.0 
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			#include "UnityShaderVariables.cginc"
			#pragma shader_feature _UVSCROLL_ON


			struct appdata
			{
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
				float4 texcoord1 : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 texcoord : TEXCOORD0;
				UNITY_VERTEX_OUTPUT_STEREO
				
			};

			uniform sampler2D _MainTex;
			uniform fixed4 _Tint;
			uniform float4 _MainTex_ST;
			uniform half _UVScrollAnimation;
			uniform half _FlipUVHorizontal;
			uniform half _FlipUVVertical;
			uniform float _ScrollByTexel;
			uniform float2 _UVScrollSpeed;
			uniform float4 _MainTex_TexelSize;
			
			v2f vert ( appdata v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				o.texcoord.xy = v.texcoord.xy;
				o.texcoord.zw = v.texcoord1.xy;
				
				// ase common template code
				
				
				v.vertex.xyz +=  float3(0,0,0) ;
				o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}
			
			fixed4 frag (v2f i ) : SV_Target
			{
				fixed4 myColorVar;
				// ase common template code
				float2 uv0_MainTex = i.texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;
				float temp_output_16_0_g3 = uv0_MainTex.x;
				float temp_output_17_0_g3 = uv0_MainTex.y;
				float2 appendResult23_g3 = (float2(temp_output_16_0_g3 , temp_output_17_0_g3));
				float2 appendResult13_g3 = (float2(lerp(temp_output_16_0_g3,( 1.0 - temp_output_16_0_g3 ),_FlipUVHorizontal) , lerp(temp_output_17_0_g3,( 1.0 - temp_output_17_0_g3 ),_FlipUVVertical)));
				float temp_output_116_0_g3 = ( _UVScrollSpeed.x * _Time.y );
				float temp_output_123_0_g3 = ( _UVScrollSpeed.y * _Time.y );
				float2 appendResult122_g3 = (float2(temp_output_116_0_g3 , temp_output_123_0_g3));
				float2 appendResult100_g3 = (float2(( _MainTex_TexelSize.x * round( temp_output_116_0_g3 ) ) , ( _MainTex_TexelSize.y * round( temp_output_123_0_g3 ) )));
				float2 appendResult14_g3 = (float2(( (lerp(appendResult122_g3,appendResult100_g3,_ScrollByTexel)).x + lerp(temp_output_16_0_g3,( 1.0 - temp_output_16_0_g3 ),_FlipUVHorizontal) ) , ( (lerp(appendResult122_g3,appendResult100_g3,_ScrollByTexel)).y + lerp(temp_output_17_0_g3,( 1.0 - temp_output_17_0_g3 ),_FlipUVVertical) )));
				#ifdef _UVSCROLL_ON
				float2 staticSwitch22_g3 = lerp(appendResult13_g3,appendResult14_g3,_UVScrollAnimation);
				#else
				float2 staticSwitch22_g3 = appendResult23_g3;
				#endif
				
				
				myColorVar = ( tex2D( _MainTex, staticSwitch22_g3 ) * _Tint );
				return myColorVar;
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
	
}
/*ASEBEGIN
Version=16705
38;83;1800;950;1715;421;1;True;True
Node;AmplifyShaderEditor.TemplateShaderPropertyNode;7;-1478,-6;Float;False;0;0;_MainTex;Shader;0;5;SAMPLER2D;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;6;-1242,142;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FunctionNode;3;-966,171;Float;False;UV Scroll;0;;3;c6ebe2788c99b854c95ad5420f494e89;0;3;16;FLOAT;0;False;17;FLOAT;0;False;95;SAMPLER2D;;False;3;FLOAT2;0;FLOAT;18;FLOAT;19
Node;AmplifyShaderEditor.SamplerNode;4;-623,-9;Float;True;Property;_TextureSample0;Texture Sample 0;1;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TemplateShaderPropertyNode;9;-508,200;Float;False;0;0;_Tint;Shader;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;8;-274,-6;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;0;0,0;Float;False;True;2;Float;ASEMaterialInspector;0;40;CG Framework/Unlit/Unlit Transparent;f5030f6f1d46cfb4a8686633d4703d2d;True;SubShader 0 Pass 0;0;0;SubShader 0 Pass 0;2;False;False;False;True;2;False;-1;False;False;True;2;False;-1;True;3;False;-1;False;True;3;RenderType=Transparent=RenderType;Queue=Transparent=Queue=0;LightMode=ForwardBase;False;0;True;2;5;False;-1;10;False;-1;0;1;False;-1;0;False;-1;False;False;False;False;False;True;2;False;-1;False;False;False;True;2;0;Diffuse;0;0;Standard;0;0;1;True;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0,0,0;False;0
WireConnection;6;2;7;0
WireConnection;3;16;6;1
WireConnection;3;17;6;2
WireConnection;3;95;7;0
WireConnection;4;0;7;0
WireConnection;4;1;3;0
WireConnection;8;0;4;0
WireConnection;8;1;9;0
WireConnection;0;0;8;0
ASEEND*/
//CHKSM=3C56C51263AA280F0B76ACAB92E44100C654D9A9